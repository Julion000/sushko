package com.eviltester.webdriver;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;

/**
 * Created by 1 on 31.05.2016.
 */
public class TestRozetka {

    private WebDriver driver;
    private String baseUrl;


    @Before
    public void SetUp() throws Exception{
        driver= new FirefoxDriver();
        baseUrl="http://rozetka.com.ua/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(baseUrl);
        driver.manage().window().maximize();
    }

    @After
    public void TearDown() throws Exception{
        driver.quit();
    }

    @Test
    public void CheckLogo() throws InterruptedException{



        Assert.assertTrue("Logo is not displayed on the page",
                driver.findElement(By.xpath(".//div/img[contains(@src,'99.png')]")).isDisplayed());
        TimeUnit.SECONDS.sleep(5);
    }
    @Test
        public void CheckAppleInMenu() throws InterruptedException {
            Assert.assertTrue("Apple is not displayed in the menu",
                    driver.findElement(By.xpath(".//*[@id='m-main']/li[2]/a[contains(text(),'Apple')]")).isDisplayed());
        }
    @Test
    public void CheckMP3InMenu() throws InterruptedException {
        Assert.assertTrue("Section with MP3 is not displayed in the menu",
                driver.findElement(By.xpath(".//*[@id='m-main']/li/a[contains(text(),'MP3')]")).isDisplayed());
    }

    @Test
    public void CheckKharkovCity() throws InterruptedException {

                driver.findElement(By.xpath(".//*[@id='city-chooser']/a")).click();
     Assert.assertTrue("Cities are not displayed", driver.findElement(By.xpath(".//div/a[contains(text(),'Харьков')]")).isDisplayed());
    }

    @Test
    public void CheckOdessaCity() throws InterruptedException {

        driver.findElement(By.xpath(".//*[@id='city-chooser']/a")).click();
        Assert.assertTrue("Cities are not displayed", driver.findElement(By.xpath(".//div/a[contains(text(),'Одесса')]")).isDisplayed());
    }

    @Test
    public void CheckKievCity() throws InterruptedException {

        driver.findElement(By.xpath(".//*[@id='city-chooser']/a")).click();
        Assert.assertTrue("Cities are not displayed", driver.findElement(By.xpath(".//div[1]/a[contains(text(),'Киев')]")).isDisplayed());
    }

    @Test
    public void CheckEmptyCart() throws InterruptedException {

        driver.findElement(By.xpath(".//a[contains(@href,'cart')]")).click();
       Assert.assertTrue("Bucket is not empty",
                driver.findElement(By.xpath(".//h2[contains(text(),'Корзина пуста')]")).isDisplayed());
    }


}
