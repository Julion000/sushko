package com.eviltester.webdriver;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.SystemClock;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by 1 on 02.06.2016.
 */
public class TestStackoverflow {

    private WebDriver driver;
    private String baseUrl;


    @Before
    public void SetUp() throws Exception{
        driver= new FirefoxDriver();
        baseUrl="http://stackoverflow.com";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(baseUrl);
        driver.manage().window().maximize();
    }
    @After
    public void TearDown() throws Exception{
        driver.quit();
    }

    @Test
    public void CheckFeaturedValue() throws Exception {
        String num = driver.findElement(By.xpath(".//*[@id='tabs']/a[@data-value='featured']/span")).getText();
        int val = Integer.parseInt(num);
        Assert.assertTrue("Value is not bigger then 300", val > 300);
    }

    @Test
    public void CheckGoogleButton() throws Exception {
        driver.findElement(By.xpath(".//a[contains(text(), 'sign up')]")).click();
        Assert.assertTrue("Google Buttons are missed",
                driver.findElement(By.xpath(".//*[@id='openid-buttons']/div/div/span[contains(text(),'Google')]|.//span[contains(text(),'Facebook')]")).isDisplayed());
    }

    @Test
    public void CheckFacebookButtons() throws Exception {
        driver.findElement(By.xpath(".//a[contains(text(), 'sign up')]")).click();
        Assert.assertTrue("Facebook Buttons are missed",
                driver.findElement(By.xpath(".//span[contains(text(),'Facebook')]")).isDisplayed());
    }
    @Test
    public void CheckArticleDate() throws Exception {
        driver.findElement(By.xpath(".//*[@id='question-mini-list']/div/div[2]/h3")).click();
        String date = driver.findElement(By.xpath(".//tr/td/p/b[contains(text(),'today')]")).getText();
        Assert.assertTrue("Article is not created today", date.contains("today"));
    }

   /* @Test
    public void test4() throws Exception {
        driver.findElement(By.xpath(".//*[@id='nav-jobs']")).click();
        String salary =driver.findElement(By.xpath(".//div[3]/div/span[contains(@class,'salary')]")).getText();
      
      //  Assert.assertTrue("Article is not created today", .contains("today"));
    }*/


}
