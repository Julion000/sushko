package Lab2;

import java.io.*;


/**
 * Created by 1 on 10.04.2016.
 */
public class Lesson3_5 {
    public static void main(String [] args) throws IOException{

        System.out.println("Введите числа для подсчета суммы(для получения результата введите слово 'сумма'):");
        int sum=0;
        double d1=0;
        BufferedReader br1=new BufferedReader(new InputStreamReader(System.in));

        while(true){
        String h1=br1.readLine();
            if (h1.equals("сумма"))
            break;
                else
             try{
                d1 = Double.parseDouble(h1);
                sum+=d1;}
             catch (NumberFormatException e){
                 System.out.println("Неверное число, замена на ноль");
                 d1=0;
             }
        }
        System.out.println("Сумма: "+sum);
    }
}
