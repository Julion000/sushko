package Lab5;

import java.util.*;

import java.lang.IllegalArgumentException;
/**
 * Created by sushko on 4/20/2016.
 */
public class MyDateList implements List {
   private Object[] ob ;
    int size = 0;



    public int size() {

        return ob.length;
    }

    public boolean isEmpty() {
        if(ob.length==0)
        return true;
        else
            return false;
    }


  @Override
   public boolean contains(Object o) {

        for (int i = 0; i < ob.length; i++) {
            if (ob[i]==o){
              return true;
            break;}
        }
        return false;
    }



    @Override
    public Iterator iterator() {
        return new MyIterator();
    }

    public int indexOf(Object o) {
        int number=-1;
        for (int i = 0; i < size; i++) {
                if (ob[i] == null){
                    number= i;
            } else {
                if (o==ob[i])
                    number= i;
            }
        }
        return number;
    }




    public Object[] toArray() {
        Object[] arr = new Object[ob.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = ob[i];
        }

        return arr;
    }



    public Object[] toArray(Object[] a) {
        Object[] arr = new Object[ob.length];
        if (a.length < ob.length) {
            for (int i = 0; i < ob.length; i++) {
                arr[i] = ob[i];
            }
            return arr;
        } else {
            for (int i = 0; i < ob.length; i++) {
                a[i] = ob[i];
            }
            for (int i = ob.length; i < a.length; i++) {
                a[i] = null;

            }
            return a;
        }
    }

    public Object get(int index) {


        return ob[index];
        }

    public boolean containsAll(Collection c) {
        return false;
    }

    public boolean addAll(int index, Collection c){

            int num = ob.length + c.size();
            Object[] tem = c.toArray();
            Object[] temArr = new Object[num];

            for (int i = 0; i < index; i++) {
                temArr[i] = ob[i];
            }
            for (int i = 0; i < c.size(); i++) {
                temArr[i+index] = tem[i];
            }
            for (int i = index + c.size(), j = 0; i < num; i++, j++) {
                temArr[i] = ob[index + j];
            }

            this.ob = new Object[temArr.length];
            this.ob = temArr;
            return true;

    }


    public boolean removeAll(Collection c) {
        try {
            Object[] tem = c.toArray();
            Object[] temArr = new Object[ob.length];
            boolean flag = false;
            for (int i = 0; i < ob.length; i++) {
                flag = false;
                for (int j = 0; j < c.size(); j++) {
                    if (ob[i] == tem[j]) {
                        flag = true;

                    }

                }
                if (flag == true) {
                    temArr[i] = ob[i];
                } else {

                    temArr[i] = null;
                }
            }
            for (int i = 0; i < temArr.length; i++) {
                if (temArr[i] != null) {
                    this.remove(temArr[i]);
                }
            }

            return true;

        } catch (Exception e) {
            return false;
        }
        }
    public Object set(int index, Object element) {
        ob[index] = element;

        return ob[index];
        }

    public boolean add(Object o) {

            int n = ob.length + 1;
        Object[] temArray = new Object[n];
            for (int i = 0; i < ob.length; i++) {
                temArray[i] = ob[i];
            }
            temArray [temArray.length - 1] = o;
            this.ob = new Object[temArray.length];
            this.ob = temArray;
            return true;

        }
    public boolean retainAll(Collection c) {
        return false;
    }

    public void clear() {
        this.ob = new Object[0];
    }
    public void add(int index, Object element) {
        int num = ob.length + 1;
        Object[] temArray = new Object[num];
        for (int i = 0; i < index; i++) {
            temArray[i] = ob[i];
        }
        temArray[index] = element;
        for (int i = index; i < num; i++) {
            temArray[i + 1] = ob[i];
        }

        }

    public Object remove(int index) {
        int n = ob.length - 1;
        Object tem = ob[index];
        Object[] temArray = new Object[n];
        for (int i = index; i < n; i++) {
            temArray[i] = ob[i + 1];
        }
        for (int i = 0; i < index; i++) {
            temArray[i] = ob[i];
        }
        return tem;
       }

    public boolean remove(Object o) {
        for (int index = 0; index < size; index++)
            if (o.equals(ob[index])) {
                int numMoved = size - index - 1;
                if (numMoved > 0)
                    System.arraycopy(ob, index + 1, ob, index, numMoved);
                ob[size - 1] = null;
                return true;
            }
        return false;
        }


    public boolean addAll(Collection c) {
        Object[] a = c.toArray();
        int numNew = a.length;
        ob = Arrays.copyOf(ob, +numNew);
        System.arraycopy(a, 0, ob, size, numNew);
        size += numNew;
        return numNew != 0;
        }




    public int lastIndexOf(Object o) {
        if (o == null) {
            for (int i = size-1; i >= 0; i--)
                if (ob[i]==null)
                return i;
            } else {
            for (int i = size-1; i >= 0; i--)
                if (o.equals(ob[i]))
                return i;
            }
        return -1;
        }

    @Override
    public ListIterator listIterator() {
        return new MyListIterator();
    }

    @Override
    public ListIterator listIterator(int index) {
        return new MyListIterator(index);
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        int n = toIndex - fromIndex;
        MyDateList tmp = new MyDateList();
        for (int i = fromIndex; i < toIndex; i++) {
            tmp.add(ob[i]);
        }
        return tmp;
    }

    private class MyIterator implements Iterator {

        private int ind;


        @Override
        public boolean hasNext() {

            return ind < ob.length;
        }

        @Override
        public Object next() {
            return ob[ind++];
        }

        @Override
        public void remove() {

        }
    }

    private class MyListIterator implements ListIterator {

        private int ind;
        private boolean flag;

        public MyListIterator() {
            ind = 0;
        }

        public MyListIterator(int i) {
            ind = i;
        }


        @Override
        public boolean hasNext() {
            return ind < ob.length;
        }

        @Override
        public Object next() {
            flag = true;
            return ob[ind++];
        }

        @Override
        public boolean hasPrevious() {
            return ind >= 0;
        }

        @Override
        public Object previous() {
            flag = true;
            return ob[ind--];
        }

        @Override
        public int nextIndex() {
            return ind++;
        }

        @Override
        public int previousIndex() {
            return ind--;
        }

        @Override
        public void remove() {

            if (flag == true) {

                int n = ob.length - 1;
                Object[] tmpArray = new Object[n];
                for (int i = 0; i < ind; i++) {
                    tmpArray[i] = ob[i];
                }

                for (int i = ind; i < n; i++) {
                    tmpArray[i] = ob[i + 1];
                }

                ob = new Object[tmpArray.length];
                ob = tmpArray;

                flag = false;

            }
        }

        @Override
        public void set(Object o) {

            ob[ind] = o;


        }


        @Override
        public void add(Object o) {

            flag = false;
            int n = ob.length + 1;
            Object[] tempArray = new Object[n];
            for (int i = 0; i < ind+1; i++) {
                tempArray[i] = ob[i];
            }
            tempArray[ind+1] = o;
            for (int i = ind+1; i < n; i++) {
                tempArray[i + 1] = ob[i];
            }

            ob = new Object[tempArray.length];
            ob = tempArray;


        }
    }
}
