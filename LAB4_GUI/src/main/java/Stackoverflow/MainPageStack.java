package Stackoverflow;

import Rozetka.MainPageRozetka;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by 1 on 06.06.2016.
 */
public class MainPageStack {
    public WebDriver driver;

    public MainPageStack(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//*[@id='tabs']/a[@data-value='featured']/span")
    public WebElement lnk_featured;

    @FindBy(xpath = ".//a[contains(text(), 'sign up')]")
    public WebElement lnk_button;

    public SignUp navigateToSignUp(){
        lnk_button.click();
        return new SignUp(driver);
    }

    @FindBy(xpath = ".//*[@id='question-mini-list']/div/div[2]/h3")
    public WebElement lnk_article;

    public Article navigateArticle(){
        lnk_article.click();
        return new Article(driver);
    }

    @FindBy(xpath = ".//*[@id='hlogo']/a")
    public WebElement lnk_logo;

    public MainPageRozetka navigateMainPage(){
        lnk_logo.click();
        return new MainPageRozetka(driver);
    }
}
