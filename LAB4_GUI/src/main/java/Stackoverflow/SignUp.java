package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by 1 on 06.06.2016.
 */
public class SignUp {
    private WebDriver driver;

    public SignUp(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//span[contains(text(),'Facebook')]")
    public WebElement lnk_facebook;
    @FindBy(xpath = ".//*[@id='openid-buttons']/div/div/span[contains(text(),'Google')]")
    public WebElement lnk_google;
}
