package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by 1 on 06.06.2016.
 */
public class City {

    private WebDriver driver;

    public City(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//*[@id='city-chooser']/a")
    public WebElement lnk_maintab_city;

    @FindBy(xpath = ".//div/a[contains(text(),'Харьков')]")
    public WebElement lnk_city_kh;
    @FindBy(xpath = ".//div/a[contains(text(),'Одесса')]")
    public WebElement lnk_city_kv;
    @FindBy(xpath = ".//div[1]/a[contains(text(),'Киев')]")
    public WebElement lnk_city_od;
}
