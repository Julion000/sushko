package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by 1 on 06.06.2016.
 */
public class MainPageRozetka {
    public WebDriver driver;
    //public MainPageStack lnk_featured;

    public MainPageRozetka(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//div/img[contains(@src,'99.png')]")
    public WebElement lnk_maintab_logo;

    @FindBy(xpath = ".//*[@id='city-chooser']/a")
    public WebElement lnk_maintab_city;

    public City navigateToCity(){
        lnk_maintab_city.click();
        return new City(driver);
    }
    @FindBy(xpath = ".//a[contains(@href, 'cart')]")
    public WebElement cartLink;

    public Basket clickCartLink() {
        cartLink.click();
        return new Basket(driver);
    }


    @FindBy(xpath = ".//*[@id='m-main']/li[2]/a[contains(text(),'Apple')]")
    public WebElement lnk_maintab_applemenu;

    @FindBy(xpath = ".//*[@id='m-main']/li/a[contains(text(),'MP3')]")
    public WebElement lnk_maintab_mp3emenu;

    @FindBy(xpath = ".//*[@id='popular_goods_2']")
    public WebElement lnk_mainpage;

}
