package patternPack.runner;
import Rozetka.Basket;
import Rozetka.City;
import Stackoverflow.MainPageStack;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import Rozetka.MainPageRozetka;
/**
 * Created by 1 on 06.06.2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/patternPack/features",
        glue = "patternPack/steps",
        tags = {"@test1Rozetka,@test2Stack"})
public class RunnerRozetka {
        public static WebDriver driver;
        public static MainPageRozetka mainPageRozetka;
    public static MainPageStack mainPageStack;
    public static City city;
    public static Basket basket;

        @BeforeClass
        public static void beforeClass(){
            driver=new FirefoxDriver();
            driver.manage().window().maximize();
            mainPageRozetka =new MainPageRozetka(driver);
            mainPageStack =new MainPageStack(driver);
        }

        @AfterClass
        public static void afterClass(){
            driver.close();
        }


}
