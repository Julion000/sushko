@test1Rozetka
Feature: check rozetka.com logo
  Scenario: check logo is present
    Given I am on rozetka main page
    Then I see Main page logo


  Scenario: check Kharkov city
    Given I am on rozetka main page
    When I click 'Выберите город' link
    Then I see 'Харьков' city



  Scenario: check Kiev city
    Given I am on rozetka main page
    When I click 'Выберите город' link
    Then I see  'Киев' city



  Scenario: check Odessa city
    Given I am on rozetka main page
    When I click 'Выберите город' link
    Then I see 'Одесса' city

  Scenario: check 'Apple' in menu
    Given I am on rozetka main page
    Then I see 'Apple' item in menu


@2testsMP3
  Scenario: check 'MP3' in menu
    Given I am on rozetka main page
    Then I see 'MP3' item in menu

  @2testsBasket
  Scenario: check Basket
    Given I am on rozetka main page
    When I click on 'Корзина' link
    Then I see 'Корзина пуста' text