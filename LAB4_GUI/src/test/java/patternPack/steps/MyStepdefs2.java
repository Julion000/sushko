package patternPack.steps;

import Rozetka.City;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import patternPack.runner.RunnerRozetka;

/**
 * Created by 1 on 08.06.2016.
 */
public class MyStepdefs2 {



    @Given("^I am on rozetka main page$")
    public void iAmOnRozetkaMainPage() throws Throwable {
        RunnerRozetka.driver.get("http://rozetka.com.ua/");
        Assert.assertTrue("It is not main page", RunnerRozetka.mainPageRozetka.lnk_mainpage.isDisplayed());

    }

    @Then("^I see Main page logo$")
    public void iSeeMainPageLogo() throws Throwable {
        Assert.assertTrue("Logo wasn't found", RunnerRozetka.mainPageRozetka.lnk_maintab_logo.isDisplayed());

    }

    @Then("^I see 'Apple' item in menu$")
    public void iSeeAppleItemInMenu() throws Throwable {
        Assert.assertTrue("Apple is not displayed in the menu", RunnerRozetka.mainPageRozetka.lnk_maintab_applemenu.isDisplayed());

    }

    @Then("^I see 'MP3' item in menu$")
    public void iSeeMPItemInMenu() throws Throwable {
        Assert.assertTrue("Section with MP3 is not displayed in the menu",RunnerRozetka.mainPageRozetka.lnk_maintab_mp3emenu.isDisplayed());
    }

    @When("^I click 'Выберите город' link$")
    public void iClickCityLink() throws Throwable {
        RunnerRozetka.city = RunnerRozetka.mainPageRozetka.navigateToCity();
        Assert.assertTrue("City link is missed", RunnerRozetka.city.lnk_maintab_city.isDisplayed());

    }

    @Then("^I see 'Харьков' city$")
    public void iSeeKharkovCity() throws Throwable {
        RunnerRozetka.city = RunnerRozetka.mainPageRozetka.navigateToCity();
        Assert.assertTrue("City link is missed", RunnerRozetka.city.lnk_city_kh.isDisplayed());

    }

    @Then("^I see  'Киев' city$")
    public void iSeeKievCity() throws Throwable {
        RunnerRozetka.city = RunnerRozetka.mainPageRozetka.navigateToCity();
        Assert.assertTrue("City link is missed", RunnerRozetka.city.lnk_city_kv.isDisplayed());

    }

    @Then("^I see 'Одесса' city$")
    public void iSeeOdessaCity() throws Throwable {
        RunnerRozetka.city = RunnerRozetka.mainPageRozetka.navigateToCity();
        Assert.assertTrue("City link is missed", RunnerRozetka.city.lnk_city_od.isDisplayed());

    }


    @When("^I click on 'Корзина' link$")
    public void iClickOnCartLink() throws Throwable {
        RunnerRozetka.basket = RunnerRozetka.mainPageRozetka.clickCartLink();
    }


    @Then("^I see 'Корзина пуста' text$")
    public void iSeeКорзинаПустаText() throws Throwable {

        RunnerRozetka.basket.CheckEmpty();
    }
}
