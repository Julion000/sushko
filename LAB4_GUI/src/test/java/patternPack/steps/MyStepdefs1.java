package patternPack.steps;

import Stackoverflow.Article;
import Stackoverflow.SignUp;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import patternPack.runner.RunnerRozetka;

/**
 * Created by sushko on 6/9/2016.
 */
public class MyStepdefs1 {
    public SignUp signup;
    public Article article;
    @Given("^I am on stackoverflow main page$")
    public void iAmOnStackoverflowMainPage() throws Throwable {
        RunnerRozetka.driver.get("http://stackoverflow.com");
        Assert.assertTrue("It is not main page", RunnerRozetka.mainPageStack.lnk_featured.isDisplayed());
    }

    @Then("^I see featured value$")
    public void iSeeFeaturedValue() throws Throwable {
        Assert.assertTrue("Value less then 300", Integer.parseInt(RunnerRozetka.mainPageStack.lnk_featured.getText())>300);

    }


    @When("^I click 'sign up' link$")
    public void iClickSignUpLink() throws Throwable {
        signup = RunnerRozetka.mainPageStack.navigateToSignUp();
        //Assert.assertTrue("City link is missed", signup.lnk_button.isDisplayed());

    }

    @Then("^I see 'Google' button$")
    public void iSeeGoogleButton() throws Throwable {
        Assert.assertTrue("Google button is absent", signup.lnk_google.isDisplayed());

    }

    @Then("^I see 'Facebook' button$")
    public void iSeeFacebookButton() throws Throwable {
        Assert.assertTrue("Facebook button is absent", signup.lnk_facebook.isDisplayed());

    }

    @When("^I click 'How to make my controller work to insert data in mongoDB' link title$")
    public void iClickQueryingArraysIntoArraysOnMongoDBLinkTitle() throws Throwable {
        article = RunnerRozetka.mainPageStack.navigateArticle();

    }

    @Then("^I see 'today' mark of creation date$")
    public void iSeeTodayMarkOfCreationDate() throws Throwable {
        Assert.assertTrue("Article is written not today", article.lnk_create.getText().contains("today"));

    }
}
