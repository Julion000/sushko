package Lab1;

/**
 * Created by 1 on 07.04.2016.
 */

public class Lesson2_3 {
    public static void main(String [] args){
        float a=1.0f;
        double b=2.0d;
        int i = (int)(float)(a);
        System.out.println("Значение переведенное из типа float в int:"+i);
        short j = (short)(float)b;
        System.out.println("Значение переведенное из типа double в short:"+j);
        float x = (float)(int)i;
        System.out.println("Значение переведенное из типа int в float:"+x);
        double y = (double)(short)j;
        System.out.println("Значение переведенное из типа short в double:"+y);
    }
}
