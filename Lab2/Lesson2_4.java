package Lab1; /**
 * Created by 1 on 07.04.2016.
 */
import java.io.*;
import java.util.Scanner;

public class Lesson2_4 {
    public static void main (String [] args)  {
        Scanner scan1= new Scanner(System.in);
        double s1 = 0;
        double s2 = 0;
            System.out.println("Введите первое число(для дробных чисел используйте запятую):");
        try{
            s1 = scan1.nextDouble();
        }
        catch (Exception e) {
        System.out.println("Вы ввели неверное значение, оно заменено на ноль");
            s1=0;
    }

           /* еще один рабочий способ обработки ошибок
           if (scan.hasNextDouble()) {
                s1 = scan.nextDouble();
            } else
                System.out.println("Вы ввели неверное значение");*/

        System.out.println("Введите математический оператор:");
        char p=0;
        try {
            p=(char)System.in.read();

        } catch  (IOException e) {
            System.out.println("Input ERROR");
        }
        Scanner scan2= new Scanner(System.in);
        System.out.println("Введите второе число(для дробных чисел используйте запятую):");
        try {
        s2 = scan2.nextDouble();
        } catch  (Exception x){
            System.out.println("Вы ввели неверное значение, оно заменено на ноль");
        }
        double t1=s1-Math.floor(s1);
        double t2=s2-Math.floor(s2);

        switch(p) {
            case '+':
                if(t1==0 & t2==0){
                    int f1=(int)s1;
                    int f2=(int)s2;
                    System.out.println(f1+f2);
                }
                else
                System.out.println(s1+s2);
                break;
            case '-':
                if(t1==0 && t2==0){
                    int f1=(int)s1;
                    int f2=(int)s2;
                    System.out.println(f1-f2);
                }
                else
                System.out.println(s1-s2);
                break;
            case '*':
                if(t1==0 && t2==0){
                    int f1=(int)s1;
                    int f2=(int)s2;
                    System.out.println(f1*f2);
                }
                else
                System.out.println(s1*s2);
                break;
            case '/':
                double res=s1/s2;
                double r=s1-Math.floor(res);
                if (r==0){
                    int d=(int)res;
                    System.out.println(res);
                }
                else
                if(s2!=0)
                System.out.println(s1/s2);
                else
                    System.out.println("Деление на ноль невозможно");
                break;
            default:
                System.out.println("Вы ввели несуществующий оператор");
                break;
        }


    }
}
