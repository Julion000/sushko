package Lab1; /**
 * Created by 1 on 09.04.2016.
 */
import java.io.*;
//import java.io.BufferedReader;
public class Class2_4 {
    public static void main(String [] args) throws IOException {

        System.out.println("Введите первое число:");
        BufferedReader br1=new BufferedReader(new InputStreamReader(System.in));
        String h1=br1.readLine();
        double d1=0;
        try {
            d1 = Double.parseDouble(h1);
        } catch (NumberFormatException e) {
            System.out.println("Неверное число, замена на ноль");
            //d1=0;
        }

        System.out.println("Введите второе число:");
        BufferedReader br2=new BufferedReader(new InputStreamReader(System.in));
        String h2=br2.readLine();
        double d2=0;
        try {
            d2 = Double.parseDouble(h2);
        } catch (NumberFormatException e) {
            System.out.println("Неверное число, замена на ноль");

        }

        char inChar;
        System.out.println("Введите оператор:");
        inChar = (char) System.in.read();

        switch(inChar) {
            case '+':
                System.out.println(d1+d2);
                break;
            case '-':
                System.out.println(d1-d2);
                break;
            case '*':
                System.out.println(d1*d2);
                break;
            case '/':
                if(d2!=0)
                    System.out.println(d1/d2);
                else
                    System.out.println("Деление на ноль невозможно");
                break;
            default:
                System.out.println("Вы ввели несуществующий оператор");
                break;
        }
    }
}
