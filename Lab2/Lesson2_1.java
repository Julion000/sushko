package Lab1;

/**
 * Created by 1 on 07.04.2016.
 */
public class Lesson2_1 {
    public static void main(String [] args){

        int a=3,b=5;
        int c=a+b;
        System.out.println("Арифмитические операции с числами а=3, b=5:");
        System.out.println("- сумма: "+c);
        c=b-a;
        System.out.println("- вычитание: "+ c);
        c=a*b;
        System.out.println("- умножение: "+c);
        c=b/a; // переменная с с типом int
        System.out.println("- деление(чисел типа int): "+c);
        float x=3, y=5;
        float d=y/x;
        System.out.println("- деление(чисел типа float): "+d);
        c=b%a;
        System.out.println("- остаток от деления: "+c);
        System.out.println("---------------------------------------------------------------");
        System.out.println("Использование операторов сравнения и логических операторов:");
        if (a>b)
            System.out.println("a>b");
        else if(a==b)
            System.out.println("a=b");
        else System.out.println("a<b");

        int f=6, g=6;
        if ((f<=g) || (a>b))
            System.out.println("Одно из условий верное (f<=g, a>b)");
        if ((f>=g)&&(a<b))
            System.out.println("Оба условия выполнены (f>=g, a<b)");
        if (f!=g)
            System.out.println("f не равно g");
        else
            System.out.println("f равно g");

    }
}
