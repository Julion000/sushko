package Lab4;
import java.util.List;
/**
 * Created by sushko on 4/14/2016.
 */
public class Race {
    public static void main (String [] args) {
        //int s=2;
        Cars[] carsArray = new Cars[3];
        carsArray[0] = new Car1(140, 0.5, 0.6, "Car1");
        carsArray[1] = new Car2(100, 0.2, 0.4, "Car2");
        carsArray[2] = new Car3(115, 0.25, 0.3, "Car3");
        Races.race(carsArray[0]);
        Races.race(carsArray[1]);
        Races.race(carsArray[2]);

       for (int i = 0; i <= carsArray.length - 1; i++) {
            for (int j = 0; j < carsArray.length- i - 1; j++) {
                if(carsArray[i].t>carsArray[i+1].t){
                    double temp=carsArray[i+1].t;
                    carsArray[i+1].t=carsArray[i].t;
                    carsArray[i].t=temp;

                }

            }
        }
        System.out.println("Машины закончили гонку в следующем порядке:");
        for (int j = 0; j <= carsArray.length - 1; j++){
            System.out.println(carsArray[j].name+": "+(int)carsArray[j].t/60+" минут "+(int)carsArray[j].t%60+" секунд");
        }

    }
}
