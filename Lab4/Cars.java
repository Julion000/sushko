package Lab4;

/**
 * Created by sushko on 4/14/2016.
 */
public abstract class Cars {
    double a;                   //ускорение
    int Vmax;                //макс скорость
    double m;                   //маневренность
    int Vo;                  // начальная скорость
    int Vcurr;              //текущая скорость
    double t;
    String name;

    public abstract void turn();//текущая скорость

    public void Convert () {
        Vo = Vo * 1000 / 3600;
        Vmax = Vmax * 1000 / 3600;
    }

    public void drive(){
        Vcurr=(int)Math.sqrt(4000*a+Vo*Vo);
        t+=((Vcurr-Vo)/a);
    }

    Cars(Cars ob){
        Vmax=ob.Vmax;
        a=ob.a;
        m=ob.m;
        name=ob.name;
    }

    Cars(int Vmax, double a, double m, String name){
        this.Vmax=Vmax;
        this.a=a;
        this.m=m;
        this.name=name;
    }
}
