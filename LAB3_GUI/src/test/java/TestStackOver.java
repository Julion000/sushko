/**
 * Created by 1 on 05.06.2016.
 */

import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import patternPack.StackOverflow.Article;
import patternPack.StackOverflow.MainPageStack;
import patternPack.StackOverflow.SignUp;

import java.util.concurrent.TimeUnit;

import static java.lang.Integer.parseInt;

public class TestStackOver {

    private static WebDriver driver;
    protected static MainPageStack mainPageStack;
    protected SignUp signup;
    protected Article article;

    @BeforeClass
    public static void SetUp(){
        driver=new FirefoxDriver();
        driver.get("http://stackoverflow.com");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        mainPageStack=new MainPageStack(driver);
    }
    @AfterClass
    public static void after(){
        driver.close();
    }

    @Test
    public void CheckFeaturedValue() throws Exception {
        mainPageStack.navigateMainPage();
        Assert.assertTrue("Value less then 300", Integer.parseInt(mainPageStack.lnk_featured.getText())>300);
    }

    @Test
    public void CheckDateArticle() throws Exception {
        mainPageStack.navigateMainPage();
        article = mainPageStack.navigateArticle();
        Assert.assertTrue("Article is not  created today", article.lnk_create.getText().contains("today"));

    }
    @Test
    public void CheckGoogle() throws Exception {
        signup = mainPageStack.navigateToSignUp();
        Assert.assertTrue("Google link is missed", signup.lnk_google.isDisplayed());
    }
    @Test
    public void CheckFacebook() throws Exception {
        signup = mainPageStack.navigateToSignUp();
        Assert.assertTrue("Facebook link is missed", signup.lnk_facebook.isDisplayed());
    }
}
