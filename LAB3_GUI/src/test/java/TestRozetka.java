import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import patternPack.City;
import patternPack.Rozetka.Basket;
import patternPack.Rozetka.MainPageRozetka;

import java.util.concurrent.TimeUnit;

/**
 * Created by 1 on 04.06.2016.
 */
public class TestRozetka {

    private static WebDriver driver;
    protected static MainPageRozetka mainPageRozetka;
    protected Basket basket;
    protected City city;

    @BeforeClass
    public static void SetUp(){
        driver=new FirefoxDriver();
        driver.get("http://rozetka.com.ua/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        mainPageRozetka=new MainPageRozetka(driver);
    }
    @AfterClass
    public static void after(){
        driver.close();
    }

    @Test
    public void CheckLogo() throws Exception{
        Assert.assertTrue("Logo wasn't found",mainPageRozetka.lnk_maintab_logo.isDisplayed());
    }
    @Test
    public void CheckAppleInMenu() throws Exception{
        Assert.assertTrue("Apple is not displayed in the menu",mainPageRozetka.lnk_maintab_applemenu.isDisplayed());
    }
    @Test
    public void CheckMP3InMenu() throws Exception{
        Assert.assertTrue("Section with MP3 is not displayed in the menu",mainPageRozetka.lnk_maintab_mp3emenu.isDisplayed());
    }
    @Test
    public void CheckCityLink() throws Exception {
        city = mainPageRozetka.navigateToCity();
        Assert.assertTrue("City link is missed", city.lnk_maintab_city.isDisplayed());
    }
    @Test
    public void CheckKharkov() throws Exception {
        city = mainPageRozetka.navigateToCity();
        Assert.assertTrue("City link is missed", city.lnk_city_kh.isDisplayed());
    }
    @Test
    public void CheckKiev() throws Exception {
        city = mainPageRozetka.navigateToCity();
        Assert.assertTrue("City link is missed", city.lnk_city_kv.isDisplayed());
    }
    @Test
    public void CheckOdessa() throws Exception {
        city = mainPageRozetka.navigateToCity();
        Assert.assertTrue("City link is missed", city.lnk_city_od.isDisplayed());
    }

   @Test
   public void test6() throws Exception {
       basket = mainPageRozetka.clickCartLink();
       basket.CheckEmpty();

   }

}

