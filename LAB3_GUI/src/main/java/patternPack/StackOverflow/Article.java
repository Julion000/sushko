package patternPack.StackOverflow;
import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * Created by 1 on 05.06.2016.
 */
public class Article {

    private WebDriver driver;

    public Article(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//tr[1]/td/p/b")
    public WebElement lnk_create;
}
