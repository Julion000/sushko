package patternPack.Rozetka;
import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 * Created by 1 on 04.06.2016.
 */
public class Basket {
    private WebDriver driver;

    public Basket(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }
    @FindBy(xpath = ".//*[@id='drop-block']/h2")
    public WebElement message;

    public void CheckEmpty() throws Exception {
        String content = message.getText();
        Assert.assertTrue("Basket is not empty", content.equals("Корзина пуста"));
    }


}
